# Information / Информация

SPEC-файл для создания RPM-пакета **postfix**.

## Install / Установка

1. Подключить репозиторий **MARKETPLACE**: `dnf copr enable marketplace/postfix`.
2. Установить пакет: `dnf install postfix`.

## Donation / Пожертвование

- [Donating](https://donating.gitlab.io/)